# estimationApp
A Shiny app translating surveillance data into disease incidence estimates

## Execution
- The app can be used remotely on [shinyapps.io](https://yo-b.shinyapps.io/incidence-estimation/).
- It can also be used locally downloading the folder `incidence-estimation/`, then opening `app.R` in RStudio.  
   It will be automatically recognised as a shinyapp and the execution button will appear to launch it.  

## Usage

After specifying epidemic parameters characterising the pathogen under study:
- **r**: the epidemic growth rate
- **&#963;**: the duration of the asymptomatic period

as well as the desired confidence on the incidence output:
- level of confidence, default to 95%
- symmetry: one-sided upper limit of CI or two-sided for an englobing interval

the user is required to fill in a sampling series in the form of a 3-column table:
- **Date**: dates at which sampling sessions occurred and host pathological status were assessed
- **N**: number of sampled host, *i.e.* number of host whose pathological status was assessed
- **M**: sampling outcome, *i.e.* number of host assessed as infected

Refer to the *Help panel* for a more detailed description.

---
Bug reports and improvement suggestions are welcomed.